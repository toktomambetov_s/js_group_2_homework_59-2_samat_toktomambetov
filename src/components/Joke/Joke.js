import React, { Component } from 'react';

class Joke extends Component {
    render() {
        return (
            <div className="Joke">
                <p>{this.props.value}</p>
            </div>
        )
    }
}

export default Joke;
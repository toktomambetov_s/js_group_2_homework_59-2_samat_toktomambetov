import React, { Component, Fragment } from 'react';
import Joke from "../../components/Joke/Joke";

class JokeList extends Component {
    state = {
        jokes: []
    };

    _makeRequest(url) {
        return fetch(url).then(response => {
            if (response.ok) {
                return response.json();
            }
            throw new Error('Something went wrong with network request');
        });
    }

    componentDidMount() {
        const JOKE_URL = 'https://api.chucknorris.io/jokes/random';

        this._makeRequest(JOKE_URL).then(joke => {
            return {...joke};
        }).then(joke => {
            const jokes = [...this.state.jokes];
            jokes.push(joke);
            this.setState({jokes});
        }).catch(error => {
           console.log(error);
        });
    };

    render() {
        return (
            <Fragment>
                <section className="Jokes">
                    {this.state.jokes.map(joke => (
                        <Joke value={joke.value}/>
                    ))}
                </section>
            </Fragment>
        );
    }
}

export default JokeList;